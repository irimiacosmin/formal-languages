

predicate TwoDisjointsSets (x: set<int>,y: set<int>){
    x*y=={}
}


predicate SetContainsX(x: set<int>,e:int)
{
e in x
}


lemma UnionContainsE( a : set<int>, b : set<int>,e:int ) 
requires SetContainsX(a,e) || SetContainsX(b,e);

ensures  SetContainsX(a+b,e);
{
  
}



function sumUntil( a:array?<int>, n:int ) : int //calculeaza suma pana in n  https://homepage.cs.uiowa.edu/~tinelli/classes/181/Spring11/Tools/Dafny/Examples/sum.dfy
  requires a != null;
  requires 0 <= n && n <= a.Length;
  decreases n;
  reads a;
{
  if (n == 0) then 0 else sumUntil(a, n-1) + a[n-1]
}


method exercise2Sum( a: array?<int> ) returns (sum: int)
  requires a != null;
  ensures sum == sumUntil(a, a.Length);
{
  var i := 0;
  sum := 0;
  while (i < a.Length)
    invariant 0 <= i && i <= a.Length;
    invariant sum == sumUntil(a, i);
    decreases a.Length - i;
  {
    sum := sum + a[i];
    i := i + 1;
  }
}

method exercise3part1( a : array<int>, b : array<int> ) returns( res:bool)
ensures res==true ==> TwoDisjointsSets(set x | x in a[0..a.Length],set x | x in b[0..b.Length])
//ensures res==false && (a.Length == 0 || b.Length ==0 )==> !TwoDisjointsSets(set x | x in a[0..a.Length],set x | x in b[0..b.Length])
{
var arrayAAsSet:=set x | x in a[0..a.Length];
var arrayseBAsSet:=set x | x in b[0..b.Length];

var DiferenceAB:=arrayAAsSet-arrayseBAsSet;
var DiferenceBA:=arrayseBAsSet-arrayAAsSet;

return (DiferenceAB==arrayAAsSet) && (DiferenceBA==arrayseBAsSet);

}



method exercise3part3(a : array<int>, b : array<int>) returns (res:bool)
requires TwoDisjointsSets(set x | x in a[0..a.Length],set x | x in b[0..b.Length])
{

var sumA:=exercise2Sum(a);
var sumB:=exercise2Sum(b);

var setA:=set x | x in a[0..a.Length];

var setB:=set x | x in b[0..b.Length];

var AhasMoreElemthanB:=false;

if(setA-setB!={})
{
AhasMoreElemthanB:=true;
}
 
 return sumA!=sumB &&AhasMoreElemthanB;
}

method Main(){
  
//TESTING ARRAY SUM

  var a := new int[5] ;
a[0], a[1], a[2], a[3], a[4] := 6,9,11,12,13 ;

var b:=exercise2Sum(a);

print b;

//TESTING DISJOINTS SUBSETS

// var a := new int[5] ;
// a[0], a[1], a[2], a[3], a[4] := 6,9,11,12,13 ;

//   var b := new int[5] ;
// b[0], b[1], b[2], b[3], b[4] := 1,2,3,4,5 ;


// var res:=exercise3part1(a,b);

// print res;

}
