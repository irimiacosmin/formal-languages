datatype Id = a | b | c
datatype Exp =
Int(i: int)
| Var(x: Id)
| Plus(e1: Exp, e2: Exp)
| Minus(e1: Exp, e2: Exp)
| Mul(e1: Exp, e2: Exp)
| Div(e1: Exp, e2: Exp)
| Lwr(e1: Exp, e2: Exp)
| LwrOrEq(e1: Exp, e2: Exp)
| Eq(e1: Exp, e2: Exp)
| NotEq(e1: Exp, e2: Exp)
| Grt(e1: Exp, e2: Exp)
| GrtOrEq(e1: Exp, e2: Exp)

datatype Stmt =
Assg(x: Id, e: Exp)
| If(expresions:Exp, StatementA:Stmt, StatementB: Stmt)
| Skip
| Seq(statementA: Stmt, StatementB: Stmt)

function method val(e: Exp, state: map<Id, int>) : int
{
match (e) {
case Int(i) => i
case Var(a) => if a in state then state[a] else 0
case Plus(e1, e2) => val(e1, state) + val(e2, state)
case Minus(e1,e2) => val(e1,state) - val(e2,state)
case Mul(e1,e2) => val(e1,state) * val(e2,state)
case Div(e1,e2) => if val(e2,state) != 0 then val(e1,state) / val(e2,state) else 0
case LwrOrEq(e1,e2) => if val(e1,state) <= val(e2,state) then 1 else 0
case Lwr(e1,e2) => if val(e1,state) < val(e2,state) then 1 else 0
case Eq(e1,e2) => if val(e1,state) == val(e2,state) then 1 else 0
case NotEq(e1,e2) => if val(e1,state) != val(e2,state) then 1 else 0
case Grt(e1,e2) => if val(e1,state) > val(e2,state) then 1 else 0
case GrtOrEq(e1,e2) => if val(e1,state) >= val(e2,state) then 1 else 0
}
}

lemma Lemma1(state: map<Id, int>)
requires state == map[a := 2, b := 0]
ensures val(Plus(Var(a), Int(5)), state) == 7
{
}

lemma LemmaMinus(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(Minus(Var(a), Var(b)), state) == 3
{
}

lemma LemmaMultiply(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(Mul(Var(a), Var(b)), state) == 18
{
}

lemma LemmaDivide(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(Div(Var(a), Var(b)), state) == 2
{
}

lemma LemmaLower(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(Lwr(Var(a), Var(b)), state) == 0
{
}

lemma LemmaLowerOrEqual(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(LwrOrEq(Var(a), Var(b)), state) == 0
ensures val(LwrOrEq(Var(a), Int(6)), state) == 1
ensures val(LwrOrEq(Var(a), Int(7)), state) == 1
{
}

lemma LemmaEqual(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(Eq(Var(a), Var(b)), state) == 0
ensures val(Eq(Var(a), Int(6)), state) == 1
ensures val(Eq(Var(b), Int(3)), state) == 1
{
}

lemma LemmaNotEqual(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(NotEq(Var(a), Var(b)), state) == 1
ensures val(NotEq(Var(a), Int(6)), state) == 0
ensures val(NotEq(Var(b), Int(3)), state) == 0
{
}

lemma LemmaGreater(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(Grt(Var(a), Var(b)), state) == 1
ensures val(Grt(Var(a), Int(6)), state) == 0
ensures val(Grt(Var(b), Int(3)), state) == 0
ensures val(Grt(Var(b), Int(2)), state) == 1
{
}

lemma LemmaGreaterOrEqual(state: map<Id, int>)
requires state == map[a := 6, b := 3]
ensures val(GrtOrEq(Var(a), Var(b)), state) == 1
ensures val(GrtOrEq(Var(a), Int(6)), state) == 1
ensures val(GrtOrEq(Var(b), Int(3)), state) == 1
ensures val(GrtOrEq(Var(b), Int(2)), state) == 1
ensures val(GrtOrEq(Var(b), Int(4)), state) == 0
{
}

function method bigStep(s: Stmt, state: map<Id, int>) : map<Id, int>
{
match (s) {
case Assg(x, e) => state[x := val(e, state)]
case If(expresions, StatementA, StatementB) => if(val(expresions, state)==1) then bigStep(StatementA, state) else bigStep(StatementB, state)
case Skip => state
case Seq(StatementA, StatementB)=> bigStep(StatementB, bigStep(StatementA, state))
}
}

method Lemma2(state: map<Id, int>)
{
var state := map[a := 2, b := 0, c := 0];
var stmts := Seq(Assg(a, Int(1)), Assg(b, Int(2)));
var algorithmOne := If(Grt(Var(a),Var(b)),Assg(c, Var(a)),Assg(c, Var(b)));
var algorithmTwo := Seq(Assg(c, Var(a)), If(Grt(Var(c), Var(b)), Assg(c, Var(b)), Skip));
assert bigStep(Assg(b, Plus(Var(a), Int(5))), state) == map[a := 2, b := 7, c := 0];
assert bigStep(If(Eq(Minus(Var(a), Int(1)), Int(1)), Assg(b, Int(2)), Assg(b, Int(3))), state) == map[a := 2, b := 2, c := 0];
assert bigStep(If(Eq(Div(Int(16), Var(a)), Int(7)), Assg(b, Int(2)), Assg(b, Int(5))), state) == map[a := 2, b := 5, c := 0];
assert bigStep(stmts, state) == map[a := 1, b := 2, c := 0];
assert bigStep(algorithmOne, state) != bigStep(algorithmTwo, state);
}